package org.smartboot.jdk;

import java.io.IOException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public abstract class AsynchronousChannelGroup {
    private final AsynchronousChannelProvider provider;

    protected AsynchronousChannelGroup(AsynchronousChannelProvider provider) {
        this.provider = provider;
    }


    public static AsynchronousChannelGroup withFixedThreadPool(int nThreads, ThreadFactory threadFactory) throws IOException {
        return AsynchronousChannelProvider.provider().openAsynchronousChannelGroup(nThreads, threadFactory);
    }

    public final AsynchronousChannelProvider provider() {
        return provider;
    }

    public abstract boolean isShutdown();

    public abstract boolean isTerminated();


    public abstract void shutdown();


    public abstract void shutdownNow() throws IOException;

    public abstract boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException;
}
