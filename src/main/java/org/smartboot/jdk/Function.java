package org.smartboot.jdk;


public interface Function<T, R> {


    R apply(T t);
}
