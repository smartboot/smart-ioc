package org.smartboot.jdk;

public interface Consumer<T> {

    void accept(T t);

}
