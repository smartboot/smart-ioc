package org.smartboot.jdk;

import org.smartboot.aio.EnhanceAsynchronousChannelProvider;

import java.io.IOException;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.concurrent.ThreadFactory;

public abstract class AsynchronousChannelProvider {


    protected AsynchronousChannelProvider() {
    }

    public static AsynchronousChannelProvider provider() {
        return ProviderHolder.provider;
    }


    public abstract AsynchronousChannelGroup openAsynchronousChannelGroup(int nThreads, ThreadFactory threadFactory) throws IOException;

    public abstract AsynchronousServerSocketChannel openAsynchronousServerSocketChannel(AsynchronousChannelGroup group) throws IOException;

    public abstract AsynchronousSocketChannel openAsynchronousSocketChannel(AsynchronousChannelGroup group) throws IOException;

    private static class ProviderHolder {
        static final AsynchronousChannelProvider provider = load();

        private static AsynchronousChannelProvider load() {
            return AccessController
                    .doPrivileged(new PrivilegedAction<AsynchronousChannelProvider>() {
                        @Override
                        public AsynchronousChannelProvider run() {
                            return new EnhanceAsynchronousChannelProvider();
                        }
                    });
        }


    }
}
