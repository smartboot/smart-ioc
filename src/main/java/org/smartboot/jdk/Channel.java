package org.smartboot.jdk;

import java.io.Closeable;
import java.io.IOException;


public interface Channel extends Closeable {


    public boolean isOpen();


    @Override
    public void close() throws IOException;

}
