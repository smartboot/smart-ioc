package org.smartboot.jdk;

import java.io.IOException;
import java.net.SocketAddress;


public abstract class AsynchronousServerSocketChannel implements AsynchronousChannel, NetworkChannel {

    public static AsynchronousServerSocketChannel open(AsynchronousChannelGroup group)
            throws IOException {
        AsynchronousChannelProvider provider = (group == null) ?
                AsynchronousChannelProvider.provider() : group.provider();
        return provider.openAsynchronousServerSocketChannel(group);
    }

    @Override
    public final AsynchronousServerSocketChannel bind(SocketAddress local) throws IOException {
        return bind(local, 0);
    }


    public abstract AsynchronousServerSocketChannel bind(SocketAddress local, int backlog) throws IOException;


    public abstract <A> void accept(A attachment, CompletionHandler<AsynchronousSocketChannel, ? super A> handler);


    @Override
    public abstract SocketAddress getLocalAddress() throws IOException;
}
