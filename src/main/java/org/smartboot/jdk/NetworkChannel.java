package org.smartboot.jdk;

import java.io.IOException;
import java.net.SocketAddress;


public interface NetworkChannel extends Channel {

    NetworkChannel bind(SocketAddress local) throws IOException;

    SocketAddress getLocalAddress() throws IOException;

}
