package org.smartboot.jdk;

import java.io.IOException;


public interface AsynchronousChannel extends Channel {

    @Override
    void close() throws IOException;
}
